const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";

var dssv = [];

var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);
if (dssvJson != null) {
  dssv = JSON.parse(dssvJson);
  for (var index = 0; index < dssv.length; index++) {
    var sv = dssv[index];
    dssv[index] = new SinhVien(
      sv.ten,
      sv.ma,
      sv.matKhau,
      sv.email,
      sv.toan,
      sv.ly,
      sv.hoa
    );
  }
  renderDSSV(dssv);
}
function themSV() {
  var newSv = layThongTinTuForm();
  var isValid =
    validation.kiemTraRong(
      newSv.ma,
      "spanMaSV",
      "Mã sinh viên không được rỗng"
    ) &&
    validation.kiemTraDoDai(
      newSv.ma,
      "spanMaSV",
      "Mã sinh viên phải gồm 4 kí tự",
      4,
      4
    );
  isValid =
    isValid &
    validation.kiemTraRong(
      newSv.ten,
      "spanTenSV",
      "Tên sinh viên không được rỗng"
    );
  if (isValid) {
    dssv.push(newSv);
    var dssvJson = JSON.stringify(dssv);
    localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
    renderDSSV(dssv);
  }
}
function xoaSinhVien(id) {
  var index = timKiemViTri(id, dssv);
  if (index != -1) {
    dssv.splice(index, 1);
    renderDSSV(dssv);
  }
}
function suaSinhVien(id) {
  var index = timKiemViTri(id, dssv);
  if (index != -1) {
    var sv = dssv[index];
    showThongTinLenForm(sv);
  }
}
